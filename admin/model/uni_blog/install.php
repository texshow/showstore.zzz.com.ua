<?php
class ModelUniBlogInstall extends Model {
	public function install() {
	
		$table = $this->db->query("show tables FROM ".DB_DATABASE." LIKE '".DB_PREFIX."blog_category'");
		$table1 = $this->db->query("show tables FROM ".DB_DATABASE." LIKE '".DB_PREFIX."uni_blog_category'");
		
		if($table->num_rows && !$table1->num_rows) {
			$query = $this->db->query("show columns FROM ".DB_PREFIX."blog_category WHERE Field = 'category_id'");

			if ($query->num_rows) { 
				$this->db->query("RENAME TABLE ".DB_PREFIX."blog_articles TO ".DB_PREFIX."uni_blog_articles");
				$this->db->query("RENAME TABLE ".DB_PREFIX."blog_articles_description TO ".DB_PREFIX."uni_blog_articles_description");
				$this->db->query("RENAME TABLE ".DB_PREFIX."blog_articles_to_category TO ".DB_PREFIX."uni_blog_articles_to_category");
				$this->db->query("RENAME TABLE ".DB_PREFIX."blog_articles_to_layout TO ".DB_PREFIX."uni_blog_articles_to_layout");
				$this->db->query("RENAME TABLE ".DB_PREFIX."blog_articles_to_related TO ".DB_PREFIX."uni_blog_articles_to_related");
				$this->db->query("RENAME TABLE ".DB_PREFIX."blog_products_to_related TO ".DB_PREFIX."uni_blog_products_to_related");
				$this->db->query("RENAME TABLE ".DB_PREFIX."blog_articles_to_store TO ".DB_PREFIX."uni_blog_articles_to_store");
				$this->db->query("RENAME TABLE ".DB_PREFIX."blog_category TO ".DB_PREFIX."uni_blog_category");
				$this->db->query("RENAME TABLE ".DB_PREFIX."blog_category_description TO ".DB_PREFIX."uni_blog_category_description");
				$this->db->query("RENAME TABLE ".DB_PREFIX."blog_category_path TO ".DB_PREFIX."uni_blog_category_path");
				$this->db->query("RENAME TABLE ".DB_PREFIX."blog_category_to_layout TO ".DB_PREFIX."uni_blog_category_to_layout");
				$this->db->query("RENAME TABLE ".DB_PREFIX."blog_category_to_store TO ".DB_PREFIX."uni_blog_category_to_store");
				$this->db->query("RENAME TABLE ".DB_PREFIX."blog_review TO ".DB_PREFIX."uni_blog_review");
			}
		}
		
	
		$this->db->query("CREATE TABLE IF NOT EXISTS `".DB_PREFIX."uni_blog_articles` (`article_id` int(11) NOT NULL AUTO_INCREMENT, `author_id` int(11) NOT NULL, `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00', `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00', `image` varchar(225) NOT NULL, `sort_order` int(3) NOT NULL DEFAULT '0', `status` tinyint(1) NOT NULL DEFAULT '1', `viewed` int(11) NOT NULL DEFAULT '0', `login_to_view` int(11) NOT NULL DEFAULT '0', PRIMARY KEY (`article_id`)) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8");
		$this->db->query("CREATE TABLE IF NOT EXISTS `".DB_PREFIX."uni_blog_articles_description` (`article_id` int(11) NOT NULL, `language_id` int(11) NOT NULL, `name` varchar(128) NOT NULL, `description` text NOT NULL, `short_description` text NOT NULL, `meta_title` varchar(255) NOT NULL, `meta_h1` varchar(255) NOT NULL, `meta_description` varchar(255) NOT NULL, `meta_keyword` varchar(255) NOT NULL, `tag` varchar(255) NOT NULL, PRIMARY KEY (`article_id`,`language_id`)) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci");
		$this->db->query("CREATE TABLE IF NOT EXISTS `".DB_PREFIX."uni_blog_articles_to_category` (`article_id` int(11) NOT NULL DEFAULT '0', `category_id` int(11) NOT NULL DEFAULT '0', PRIMARY KEY (`article_id`,`category_id`)) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci");
		$this->db->query("CREATE TABLE IF NOT EXISTS `".DB_PREFIX."uni_blog_articles_to_layout` (`article_id` int(11) NOT NULL, `store_id` int(11) NOT NULL, `layout_id` int(11) NOT NULL, PRIMARY KEY (`article_id`,`store_id`)) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci");
		$this->db->query("CREATE TABLE IF NOT EXISTS `".DB_PREFIX."uni_blog_articles_to_related` (`article_id` int(11) NOT NULL, `related_id` int(11) NOT NULL, PRIMARY KEY (`article_id`,`related_id`)) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci");
		$this->db->query("CREATE TABLE IF NOT EXISTS `".DB_PREFIX."uni_blog_products_to_related` (`article_id` int(11) NOT NULL DEFAULT '0', `product_id` int(11) NOT NULL DEFAULT '0', PRIMARY KEY (`article_id`,`product_id`)) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci");
		//$this->db->query("CREATE TABLE IF NOT EXISTS `".DB_PREFIX."product_to_blog_articles` (`article_id` int(11) NOT NULL DEFAULT '0', `product_id` int(11) NOT NULL DEFAULT '0', PRIMARY KEY (`article_id`,`product_id`)) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci");
		$this->db->query("CREATE TABLE IF NOT EXISTS `".DB_PREFIX."uni_blog_articles_to_store` (`article_id` int(11) NOT NULL AUTO_INCREMENT, `store_id` int(11) NOT NULL, PRIMARY KEY (`article_id`,`store_id`)) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci");
		$this->db->query("CREATE TABLE IF NOT EXISTS `".DB_PREFIX."uni_blog_category` (`category_id` int(11) NOT NULL AUTO_INCREMENT, `image` varchar(255) DEFAULT NULL, `parent_id` int(11) NOT NULL DEFAULT '0', `top` tinyint(1) NOT NULL, `column` int(3) NOT NULL, `sort_order` int(3) NOT NULL DEFAULT '0', `status` tinyint(1) NOT NULL, `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00', `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00', PRIMARY KEY (`category_id`)) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8");
		$this->db->query("CREATE TABLE IF NOT EXISTS `".DB_PREFIX."uni_blog_category_description` (`category_id` int(11) NOT NULL, `language_id` int(11) NOT NULL, `name` varchar(255) NOT NULL, `description` text NOT NULL, `short_description` text NOT NULL, `meta_title` varchar(255) NOT NULL, `meta_h1` varchar(255) NOT NULL, `meta_description` varchar(255) NOT NULL, `meta_keyword` varchar(255) NOT NULL, PRIMARY KEY (`category_id`,`language_id`)) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci");
		$this->db->query("CREATE TABLE IF NOT EXISTS `".DB_PREFIX."uni_blog_category_path` (`category_id` int(11) NOT NULL, `path_id` int(11) NOT NULL, `level` int(11) NOT NULL, PRIMARY KEY (`category_id`,`path_id`)) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci");
		$this->db->query("CREATE TABLE IF NOT EXISTS `".DB_PREFIX."uni_blog_category_to_layout` (`category_id` int(11) NOT NULL, `store_id` int(11) NOT NULL, `layout_id` int(11) NOT NULL, PRIMARY KEY (`category_id`,`store_id`)) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci");
		$this->db->query("CREATE TABLE IF NOT EXISTS `".DB_PREFIX."uni_blog_category_to_store` (`category_id` int(11) NOT NULL, `store_id` int(11) NOT NULL, PRIMARY KEY (`category_id`,`store_id`)) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci");
		$this->db->query("CREATE TABLE IF NOT EXISTS `".DB_PREFIX."uni_blog_review` (`review_id` int(11) NOT NULL AUTO_INCREMENT, `article_id` int(11) NOT NULL, `customer_id` int(11) NOT NULL, `author` varchar(64) NOT NULL, `text` text NOT NULL, `admin_reply` text NOT NULL, `rating` int(1) NOT NULL, `status` tinyint(1) NOT NULL DEFAULT '0', `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00', PRIMARY KEY (`review_id`)) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8");
  
		$query = $this->db->query("SELECT store_id FROM `".DB_PREFIX."setting` WHERE `code` LIKE 'blog' LIMIT 1;");
		if ($query->num_rows == 0) {
			$this->db->query("INSERT INTO `".DB_PREFIX."setting` (`setting_id`, `store_id`, `code`, `key`, `value`, `serialized`) VALUES ('', 0, 'blog', 'blog_catalog_limit', '15', 0), ('', 0, 'blog', 'blog_show_author', '1', 0), ('', 0, 'blog', 'blog_show_viewed', '1', 0), ('', 0, 'blog', 'blog_show_date_added', '1', 0), ('', 0, 'blog', 'blog_show_date_modified', '1', 0), ('', 0, 'blog', 'blog_review_status', '1', 0), ('', 0, 'blog', 'blog_image_category_width', '150', 0), ('', 0, 'blog', 'blog_image_category_height', '150', 0), ('', 0, 'blog', 'blog_image_thumb_width', '150', 0), ('', 0, 'blog', 'blog_image_thumb_height', '150', 0), ('', 0, 'blog', 'blog_image_article_width', '150', 0), ('', 0, 'blog', 'blog_image_article_height', '150', 0), ('', 0, 'blog', 'blog_image_related_width', '150', 0), ('', 0, 'blog', 'blog_image_related_height', '150', 0)");
		}

		$stores = array(0);

		$query_store = $this->db->query("SELECT store_id FROM `" . DB_PREFIX . "store`");

		foreach ($query_store->rows as $store) {
			$stores[] = $store['store_id'];
		}
		
		$newRoutes = array();

		$newRoutes[] = array(
			'name'	=> 'Blog-Category',
			'route'	=> 'uni_blog/category'
		);
		
		$newRoutes[] = array(
			'name'	=> 'Blog-Article',
			'route'	=> 'uni_blog/article'
		);

		foreach ($newRoutes as $newRoute) {
			$query_name = $this->db->query("SELECT layout_id FROM `" . DB_PREFIX . "layout` WHERE `name` LIKE '" . $newRoute['name'] . "' LIMIT 1");
			 
			//$this->log->write($query_name);
			
			if ($query_name->num_rows == 0) {
				$this->db->query("INSERT INTO `" . DB_PREFIX . "layout` SET `name`= '" . $newRoute['name'] . "'");
			}
			
			foreach ($stores as $store_id) {
				$query = $this->db->query("SELECT layout_id FROM `" . DB_PREFIX . "layout_route` WHERE `store_id`= '" . (int)$store_id . "' AND `route` LIKE '" . $newRoute['route'] . "' LIMIT 1");
				if ($query->num_rows == 0) {
					$this->db->query("INSERT INTO `" . DB_PREFIX . "layout_route` SET `layout_id`= (SELECT layout_id FROM `" . DB_PREFIX . "layout` WHERE `name` LIKE '" . $newRoute['name'] . "' LIMIT 1), `store_id`='" . (int)$store_id . "', `route`='" . $newRoute['route'] . "'");
				}
			}
		}

	}
}
?>